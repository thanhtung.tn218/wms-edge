package com.viettelpost.platform.wms.edge.job;

import com.viettelpost.platform.root.common.quarkus.job.JobAbs;
import com.viettelpost.platform.root.common.quarkus.service.HazelcastService;
import com.viettelpost.platform.root.common.utils.ReactiveConverter;
import com.viettelpost.platform.wms.common.caller.CoreServiceCaller;
import com.viettelpost.platform.wms.common.caller.RcsAgvCaller;
import io.quarkus.scheduler.Scheduled;
import io.smallrye.mutiny.Uni;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import reactor.core.publisher.Mono;

@ApplicationScoped
@Slf4j
@Named("FetchCommand")
public class JobFetchCommand extends JobAbs<Void> {

    @ConfigProperty(name = "job.cron.fetch-command.period")
    String periodConfig;

    @ConfigProperty(name = "internal.core.api.token")
    String coreToken;

    @Inject
    CoreServiceCaller coreServiceCaller;

    @Inject
    RcsAgvCaller rcsAgvCaller;

    public JobFetchCommand(HazelcastService hazelcastService) {
        super(hazelcastService);
    }

    @Scheduled(cron = "${job.cron.fetch-command.period}")
    public Uni<Void> scheduleFetchCommands() {
        return ReactiveConverter.toUni(super.executeTask("scheduleFetchCommands", periodConfig));
    }

    @Override
    protected Mono<Void> taskAction() {
        log.debug("JobFetchCommand.execute()");
        return coreServiceCaller.fetchCommands(coreToken)
            .flatMap(deviceCommand -> switch (deviceCommand.getActionType()) {
                case IN -> rcsAgvCaller.finishRCSLocations(deviceCommand.getCommandInfo());
                case OUT -> rcsAgvCaller.sendRCSLocations(deviceCommand.getCommandInfo());
            })
            .then();
    }
}
