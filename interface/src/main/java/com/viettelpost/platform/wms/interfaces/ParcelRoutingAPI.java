package com.viettelpost.platform.wms.interfaces;

import co.elastic.apm.api.Traced;
import com.viettelpost.platform.root.common.utils.ReactiveConverter;
import com.viettelpost.platform.wms.service.handler.ParcelRoutingService;
import io.smallrye.mutiny.Uni;
import javax.validation.Valid;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;


@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Path("parcel")
@Tag(name = "Chute management")
@RequiredArgsConstructor
@Slf4j
public class ParcelRoutingAPI {

    final ParcelRoutingService parcelRoutingService;

    @GET
    @Path("destination")
    @Operation(summary = """
        API-01
        The WCS system sends the ParcelCode (Barcode/PackCode/RFID/TOTE…), ScannerType Device type, Device ID,\s
        the Induction number and the ScannerType to the WMS system then the WMS system replies the destionation\s
        information to the WCS.
        """)
    @APIResponse(responseCode = "401", description = "Unauthorized")
    @APIResponse(responseCode = "400", description = "Bad request")
    @APIResponse(responseCode = "403", description = "Forbidden")
    @APIResponse(responseCode = "200", description = "OK")
    @Traced
    public Uni<Object> getParcelDestinationApi(@Context HttpHeaders headers,
        @Valid @BeanParam Object reqDto) {
        return ReactiveConverter.toUni(parcelRoutingService.getParcelDestination(reqDto));
    }

}
