package com.viettelpost.platform.wms.interfaces;

import co.elastic.apm.api.Traced;
import com.viettelpost.platform.wms.service.handler.HealthCheckService;
import io.smallrye.mutiny.Uni;
import io.smallrye.mutiny.converters.uni.UniReactorConverters;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import lombok.RequiredArgsConstructor;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

@Path("")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tag(name = "Health1")
@RequiredArgsConstructor
public class HealthCheckAPI {

    final HealthCheckService healthCheckService;

    @GET
    @Path(("/health"))
    @Traced
    public Uni<String> checkHealthApi(@Context HttpHeaders headers) {
        return Uni.createFrom().converter(UniReactorConverters.fromMono(), healthCheckService.checkHealth());
    }
}
