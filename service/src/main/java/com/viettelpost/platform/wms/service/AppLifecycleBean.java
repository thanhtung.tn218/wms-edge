package com.viettelpost.platform.wms.service;

import io.quarkus.runtime.ShutdownEvent;
import io.quarkus.runtime.StartupEvent;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import lombok.extern.slf4j.Slf4j;

@ApplicationScoped
@Slf4j
public class AppLifecycleBean {

    void onStart(@Observes StartupEvent ev) {
        log.info("[STARTUP] The application wms is starting...");
    }

    void onStop(@Observes ShutdownEvent ev) {
        log.info("[STARTUP] The application wms is stopping...");
    }

}
