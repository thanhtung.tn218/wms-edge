package com.viettelpost.platform.wms.service.handler;

import reactor.core.publisher.Mono;

public interface HealthCheckService {

    Mono<String> checkHealth();
}
