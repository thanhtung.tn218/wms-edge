package com.viettelpost.platform.wms.service.handler.impl;
import com.viettelpost.platform.root.common.quarkus.cache.CacheValues;
import com.viettelpost.platform.wms.service.handler.HealthCheckService;
import javax.enterprise.context.Dependent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@Slf4j
@Dependent
@RequiredArgsConstructor
public class HealthCheckHandler implements HealthCheckService {

    private final CacheValues<String, String> cacheValues;


    @Override
    public Mono<String> checkHealth() {
        return cacheValues.getData("status");
    }

}
