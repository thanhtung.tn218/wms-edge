package com.viettelpost.platform.wms.service.handler;
import reactor.core.publisher.Mono;

public interface ParcelRoutingService {

    Mono<Object> getParcelDestination(Object reqDto);

}
