package com.viettelpost.platform.wms.service.handler.impl;

import com.viettelpost.platform.wms.service.handler.ParcelRoutingService;
import javax.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
class ParcelRouting implements ParcelRoutingService {

    @Override
    public Mono<Object> getParcelDestination(Object reqDto) {
        log.info(reqDto + "Receive request to get parcel destination of {}", reqDto);
        return Mono.just(new Object());
    }
}
