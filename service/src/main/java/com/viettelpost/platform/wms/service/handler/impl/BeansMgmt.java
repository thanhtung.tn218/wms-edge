package com.viettelpost.platform.wms.service.handler.impl;

import com.viettelpost.platform.wms.service.handler.BeanService;
import java.util.Set;
import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Any;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.util.AnnotationLiteral;
import javax.inject.Inject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;

@SuppressWarnings("unused")
@Dependent
@RequiredArgsConstructor
@Slf4j
public class BeansMgmt implements BeanService {

    @Inject
    BeanManager beanManager;

    @Override
    public Flux<String> createdBeansName() {
        return Flux.fromIterable(createdBeans()).mapNotNull(bean -> bean.getBeanClass().getName());
    }

    public Set<Bean<?>> createdBeans() {
        return beanManager.getBeans(Object.class, new AnnotationLiteral<Any>() {
        });
    }
}
