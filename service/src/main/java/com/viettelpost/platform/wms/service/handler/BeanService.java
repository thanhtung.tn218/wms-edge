package com.viettelpost.platform.wms.service.handler;

import reactor.core.publisher.Flux;

public interface BeanService {

    Flux<String> createdBeansName();
}
