package com.viettelpost.platform.wms.configuration;

import javax.enterprise.context.Dependent;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;

@SuppressWarnings("unused")
@Slf4j
public class Configuration {

    @Dependent
    ModelMapper getModelMapper() {
        return new ModelMapper();
    }
}
