############ Builder Stage Parent ###################
FROM maven:3.8.7 AS builder

WORKDIR /server/root-parent
ADD ./root-parent /server/root-parent
RUN --mount=type=cache,target=/root/.m2 mvn install -Dmaven.test.skip=true -e

WORKDIR /server/root-common
ADD ./root-common /server/root-common
RUN --mount=type=cache,target=/root/.m2 mvn install -Dmaven.test.skip=true

WORKDIR /server/wms
ADD ./ /server/wms
RUN --mount=type=cache,target=/root/.m2 mvn install -DskipTests

########### Run stage #####################
FROM ghcr.io/graalvm/jdk:ol9-java17
ENV LANG='en_US.UTF-8' LANGUAGE='en_US:en'ENV TZ=Asia/Ho_Chi_Minh
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime
#ENV JAVA_OPTIONS
RUN microdnf install telnet ca-certificates \
    && microdnf update \
    && microdnf clean all \
    && mkdir /deployments \
    && chown 1001 /deployments \
    && chmod "g+rwX" /deployments \
    && chown 1001:root /deployments \
    && echo "securerandom.source=file:/dev/urandom" >> /usr/lib64/graalvm/graalvm22-ce-java17/lib/security/java.security

WORKDIR /deployments
COPY --from=builder /server/wms/main/target/ /deployments/
COPY --from=builder /server/wms/elastic-apm-agent-1.28.0.jar /deployments/elastic-apm-agent-1.28.0.jar

EXPOSE 8100
USER 1001

ENTRYPOINT exec java -javaagent:/deployments/elastic-apm-agent-1.28.0.jar $JAVA_OPTIONS -Dquarkus.config.locations=resources/application.properties -jar /deployments/wms-edge-1.0-runner.jar
