package com.viettelpost.platform.wms.main;

import co.elastic.apm.attach.ElasticApmAttacher;
import io.quarkus.runtime.Quarkus;
import io.quarkus.runtime.annotations.QuarkusMain;

@QuarkusMain
public class Main {

    public static void main(String[] args) {
        Quarkus.run(args);
        ElasticApmAttacher.attach();
    }
}
